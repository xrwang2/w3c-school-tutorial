﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragCube : MonoBehaviour
{
    public float speed = 10;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDrag()
    {
        Vector3 oldPosition = transform.position;
        transform.position = new Vector3(oldPosition.x + speed * Input.GetAxis("Mouse X") * Time.deltaTime,
            oldPosition.y + speed * Input.GetAxis("Mouse Y") * Time.deltaTime,
            oldPosition.z);
    }
}
