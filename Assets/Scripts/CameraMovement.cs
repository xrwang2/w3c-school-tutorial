﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    /// <summary>
    /// 移动速度
    /// </summary>
    public float speed = 0.5f;
    /// <summary>
    /// 最小的Z值
    /// </summary>
    public float minZ = -20;
    /// <summary>
    /// 最大的Z值
    /// </summary>
    public float maxZ = 0;
    /// <summary>
    /// 是否靠近英雄
    /// </summary>
    public bool closeToHero = true;
    /// <summary>
    /// 英雌的变换
    /// </summary>
    private Transform heroTransform;
    /// <summary>
    /// 摄像机到英雄的距离
    /// </summary>
    private float distance;
    /// <summary>
    /// 弧度速度
    /// </summary>
    private float radianSpeed;


    // Start is called before the first frame update
    void Start()
    {
        GameObject hero = GameObject.FindGameObjectWithTag("Player");
        heroTransform = hero.transform;
        distance = (transform.position - heroTransform.position).magnitude;
        radianSpeed = speed / distance;
    }

    // Update is called once per frame
    void Update()
    {
        //GetCloseOrAway();
        Encircle();
    }

    /// <summary>
    /// 靠近或者远离
    /// </summary>
    private void GetCloseOrAway()
    {
        Vector3 position = transform.position;
        float z = position.z + speed * Time.deltaTime * (closeToHero ? 1 : -1);
        if (z > maxZ)
        {
            z = maxZ;
            closeToHero = !closeToHero;
        }
        else if (z < minZ)
        {
            z = minZ;
            closeToHero = !closeToHero;
        }
        transform.position = new Vector3(position.x, position.y, z);
    }

    /// <summary>
    /// 环绕
    /// </summary>
    private void Encircle()
    {
        /*Vector3 position = transform.position;
        float radian = Mathf.Atan2(position.z, position.x);
        radian += radianSpeed * Time.deltaTime;
        float x = distance * Mathf.Cos(radian);
        float z = distance * Mathf.Sin(radian);
        transform.position = new Vector3(x, position.y, z);*/
        transform.RotateAround(heroTransform.position, Vector3.up, -1 * radianSpeed * 180 / Mathf.PI * Time.deltaTime);
    }
}