﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NaviToPery : MonoBehaviour
{
    public Transform target;

    // Start is called before the first frame update
    void Start()
    {
        if (target == null)
        {
            GameObject prey = GameObject.Find("Prey");
            if (prey != null)
                target = prey.transform;
        }
        if (target == null)
            Debug.Log("没有导航目标。");
        else
            GetComponent<NavMeshAgent>().destination = target.position;
    }

    // Update is called once per frame
    void Update()
    {

    }
}