﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CreateGameObject : MonoBehaviour
{
    /// <summary>
    /// 主摄像机的名称
    /// </summary>
    public const string CAMERA_MAIN_NAME = "Main Camera";
    /// <summary>
    /// 第一人称视角摄像机的名称
    /// </summary>
    public const string CAMERA_FPP_NAME = "CameraFPP";

    /// <summary>
    /// 主摄像机（环绕）
    /// </summary>
    private Camera cameraMain;
    /// <summary>
    /// 第一人称视角摄像机
    /// </summary>
    private Camera cameraFPP;

    /// <summary>
    /// 树的X、Z坐标范围
    /// </summary>
    private float minX;
    private float maxX;
    private float minZ;
    private float maxZ;
    /// <summary>
    /// 随机
    /// </summary>
    private System.Random random;

    // Start is called before the first frame update
    void Start()
    {
        random = new System.Random();
        GameObject plane = GameObject.Find("Plane");
        maxX = plane.transform.localScale.x;
        minX = -1 * maxX;
        maxZ = plane.transform.localScale.z;
        minZ = -1 * maxZ;
        if (Camera.allCamerasCount > 0)
        {
            cameraMain = Camera.allCameras.FirstOrDefault(c => c.name == CAMERA_MAIN_NAME);
            cameraFPP = Camera.allCameras.FirstOrDefault(c => c.name == CAMERA_FPP_NAME);
        }
        else
        {
            cameraMain = null;
            cameraFPP = null;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// 显示两个按钮，并分别在按钮事件中创建正方体和球体
    /// </summary>
    private void OnGUI()
    {
        if (GUILayout.Button("create cube 创建立方体",GUILayout.Width(150), GUILayout.Height(50)))
        {
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.AddComponent<Rigidbody>();
            cube.GetComponent<Renderer>().material.color = Color.blue;
            cube.transform.position = new Vector3(1, 2, 1);
        }
        if (GUILayout.Button("创建球体", GUILayout.Width(150), GUILayout.Height(50)))
        {
            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.AddComponent<Rigidbody>();
            sphere.GetComponent<Renderer>().material.color = Color.red;
            sphere.AddComponent<BoxCollider>();
            sphere.transform.position = new Vector3(2, 3, 1);
        }
        if (GUILayout.Button("植树", GUILayout.Height(50)))
        {
            string treeName = string.Format("tree0{0}", random.Next(1, 4));
            GameObject tree = GameObject.Find(treeName);
            GameObject newTree = Instantiate(tree);
            float x = (float)(minX + random.NextDouble() * (maxX - minX));
            float z = (float)(minZ + random.NextDouble() * (maxZ - minZ));
            newTree.transform.position = new Vector3(x, 0, z);
        }
        if (GUILayout.Button("环绕摄像机", GUILayout.Width(150), GUILayout.Height(50)))
        {
            SetCamera(true);
        }
        if (GUILayout.Button("第一人称视角摄像机", GUILayout.Width(150), GUILayout.Height(50)))
        {
            SetCamera(false);
        }
    }

    /// <summary>
    /// 设置摄像机
    /// </summary>
    /// <param name="isSurround"></param>
    private void SetCamera(bool isSurround)
    {
        if (cameraMain != null)
            cameraMain.enabled = isSurround;
        if (cameraFPP != null)
            cameraFPP.enabled = !isSurround;
    }
}