﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroAnimator : MonoBehaviour
{
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        if (animator == null)
            Debug.Log("获取英雄的动画组件失败。");
    }
    
    void Update()
    {
        if (animator == null)
            return;
        if (Input.GetKeyDown(KeyCode.Alpha1))
            animator.Play("idle");
        else if (Input.GetKeyDown(KeyCode.Alpha2))
            animator.Play("charge");
        else if (Input.GetKeyDown(KeyCode.Alpha3))
            animator.Play("walk");
        else if (Input.GetKeyDown(KeyCode.Alpha4))
            animator.Play("attack");
    }
}
