﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddForce : MonoBehaviour
{
    public float forceStrenth = 1f;

    private Rigidbody rigidBody;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnMouseDrag()
    {
        if (rigidBody == null)
        {
            Debug.Log("no rigidbody found.");
            return;
        }
        rigidBody.AddForce(Input.GetAxis("Mouse X") * Time.deltaTime * forceStrenth, Input.GetAxis("Mouse Y") * Time.deltaTime * forceStrenth, 0,ForceMode.VelocityChange);
    }
}