﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    /// <summary>
    /// 角色控制器
    /// </summary>
    CharacterController m_characterController;
    /// <summary>
    /// 移动速度
    /// </summary>
    public float m_moveSpeed = 3f;
    /// <summary>
    /// 重力
    /// </summary>
    public float m_gravity = 2f;
    /// <summary>
    /// 旋转速度
    /// </summary>
    public float m_rotateSpeed = 0.1f;
    /// <summary>
    /// 第一人称视角摄像机
    /// </summary>
    private Camera cameraFPP;

    // Start is called before the first frame update
    void Start()
    {
        m_characterController = GetComponent<CharacterController>();
        cameraFPP = (Camera)FindObjectOfType(typeof(Camera));
    }

    // Update is called once per frame
    void Update()
    {
        //Move();
        Rotate();
    }

    /// <summary>
    /// 移动
    /// </summary>
    private void Move()
    {
        float dx = Input.GetAxis("Horizontal");
        float dy = -1;
        float dz = Input.GetAxis("Vertical");
        m_characterController.Move(transform.TransformDirection(dx, dy, dz) * m_moveSpeed * Time.deltaTime);
    }

    /// <summary>
    /// 旋转
    /// </summary>
    private void Rotate()
    {
        float dx = Input.GetAxis("Mouse X");
        if (dx == 0f)
            return;
        float dy = 0f;// Input.GetAxis("Mouse Y");
        transform.rotation = Quaternion.LookRotation(new Vector3(dx, 0, dy) * m_rotateSpeed * Time.deltaTime);
        cameraFPP.transform.rotation = transform.rotation;
    }
}